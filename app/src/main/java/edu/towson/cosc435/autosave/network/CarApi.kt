package edu.towson.cosc435.autosave.network

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import edu.towson.cosc435.autosave.controllers.CarController
import edu.towson.cosc435.autosave.models.Car
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONArray
import java.net.URL
import java.util.*

interface ICarApi{
    suspend fun fetchCars(): Deferred<List<Car>>
    suspend fun fetchImage(imageUrl: String): Deferred<Bitmap>
}

class CarApi(val controller: CarController):ICarApi {

    private val BASE_URL: String = "https://my-json-server.typicode.com/ddrastic14/AutoSaveDB/cars"

    private val client: OkHttpClient = OkHttpClient()

    override suspend fun fetchCars(): Deferred<List<Car>> {
        return controller.async(Dispatchers.IO) {
            val request = Request.Builder()
                .url(BASE_URL)
                .get()
                .build()
            val result:String? = client.newCall(request).execute().body?.string()
            val cars: List<Car> = parseJson(result)
            cars

        }


    }

    override suspend fun fetchImage(imageUrl: String): Deferred<Bitmap> {
        return controller.async(Dispatchers.IO){
            val fileName = getImageFileName(imageUrl)
            val bitmap = controller.checkCache(fileName)
            if(bitmap != null){
                bitmap
            }else{
                val request = Request.Builder()
                    .url(imageUrl)
                    .get()
                    .build()
                val stream = client.newCall(request).execute().body?.byteStream()
                val bitmap = BitmapFactory.decodeStream(stream)


                bitmap
            }

        }
    }

    private fun parseJson(json:String?):List<Car>{
        val cars = mutableListOf<Car>()
        if (json == null) return cars
        val jsonArr = JSONArray(json)
        var i = 0
        while(i<jsonArr.length()){
            val jsonObj = jsonArr.getJSONObject(i)
            val car = Car(
                id = UUID.fromString(jsonObj.getString("id")),
                make = jsonObj.getString("make"),
                model = jsonObj.getString("model"),
                bodyType = jsonObj.getString("bodyType"),
                year = jsonObj.getString("year"),
                imageUrl = jsonObj.getString("imageUrl"),
                price = jsonObj.getString("price"),
                mileage = jsonObj.getString("mileage"),
                location = jsonObj.getString("location"),
                color = jsonObj.getString("color"),
                driveType = jsonObj.getString("driveType"),
                transmission = jsonObj.getString("transmission"),
                engine = jsonObj.getString("engine"),
                fuel = jsonObj.getString("fuel"),
                MPG = jsonObj.getString("MPG"),
                VIN = jsonObj.getString("VIN"),
                stockNum = jsonObj.getString("stockNum"),
                mechanic = jsonObj.getString("mechanic"),
                carFax = jsonObj.getString("carFax"),
                review = jsonObj.getString("review"),
                dealerPhone = jsonObj.getString("dealerPhone"),
                savedCars = jsonObj.getBoolean("savedCars")


            )
            cars.add(car)
            i++
        }
        return cars
    }

    private fun getImageFileName(url:String):String{
        val urlObj = url
        return urlObj


    }

}