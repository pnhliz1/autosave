package edu.towson.cosc435.autosave.fragments

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.fragment.app.Fragment
import edu.towson.cosc435.autosave.R
import edu.towson.cosc435.autosave.controllers.CarController
import edu.towson.cosc435.autosave.models.Car
//import kotlinx.android.synthetic.main.activity_view_car.*
//import kotlinx.android.synthetic.main.activity_view_car.carImage
//import kotlinx.android.synthetic.main.activity_view_car.carMake
//import kotlinx.android.synthetic.main.activity_view_car.carModel
//import kotlinx.android.synthetic.main.activity_view_car.carYear
//import kotlinx.android.synthetic.main.activity_view_car_fragment.*
import kotlinx.android.synthetic.main.listcar_layout.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception
import java.util.*
import java.util.jar.Manifest
import android.Manifest.permission
import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.location.Location
import android.location.LocationManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat.getSystemService
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import kotlinx.android.synthetic.main.activity_view_car_fragment.*
import kotlinx.android.synthetic.main.listcar_layout.*
import kotlinx.android.synthetic.main.listcar_layout.carImage


class ViewCarFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(edu.towson.cosc435.autosave.R.layout.activity_view_car_fragment, container, false)
    }

    private lateinit var carController: CarController

    override fun onAttach(context: Context) {
        super.onAttach(context)

        when(context) {
            is CarController -> carController = context
            else -> throw Exception("CarController expected")
        }
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val car = carController.getCarData()
        populateCarForm(car,carController)



        dealerPhone.setOnClickListener{handlePhoneClick(car)}
        carFax.setOnClickListener{handleCarFaxClick(car)}
        review.setOnClickListener{handleReviewsClick(car)}
        findMechanic.setOnClickListener{handleMechanicClick(car)}
    }




    private fun handlePhoneClick(car:Car?){

        val intent = Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + car?.dealerPhone.toString()));
        val permissionCheck =
            ContextCompat.checkSelfPermission(activity!!, android.Manifest.permission.CALL_PHONE)
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {


                ActivityCompat.requestPermissions(
                    activity!!,
                    arrayOf(android.Manifest.permission.CALL_PHONE),
                    PERMISSION_REQUEST
                )
            }


        else{
            startActivity(intent)
        }

    }

    private fun handleCarFaxClick(car:Car?){

        val intent = Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(""+ car?.carFax.toString()));
        val permissionCheck =
            ContextCompat.checkSelfPermission(activity!!, android.Manifest.permission.INTERNET)
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {


            ActivityCompat.requestPermissions(
                activity!!,
                arrayOf(android.Manifest.permission.INTERNET),
                PERMISSION_REQUEST
            )
        }


        else{
            startActivity(intent)
        }

    }

    private fun handleReviewsClick(car:Car?){

        val intent = Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("" + car?.review.toString()));
        val permissionCheck =
            ContextCompat.checkSelfPermission(activity!!, android.Manifest.permission.INTERNET)
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {


            ActivityCompat.requestPermissions(
                activity!!,
                arrayOf(android.Manifest.permission.INTERNET),
                PERMISSION_REQUEST
            )
        }


        else{
            startActivity(intent)
        }

    }

    private fun handleMechanicClick(car:Car?){

        val gmmIntentUri = Uri.parse("geo:"+car?.mechanic.toString()+"?q=mechanics")
        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        mapIntent.setPackage("com.google.android.apps.maps")
        startActivity(mapIntent)

    }

    private fun populateCarForm(car: Car?,controller:CarController) {
        if(car != null) {

            carMake.text = car?.make
            carModel.text = car?.model
            carYear.text = car?.year
            vehiclePrice.text = car?.price
            vehicleMileage.text = car?.mileage
            vehicleLocation.text = car?.location
            vehicleColor.text = car?.color
            driveType.text = car?.driveType
            Transmission.text = car?.transmission
            bodyStyle.text = car?.bodyType
            engine.text = car?.engine
            fuel.text = car?.fuel
            mpg.text = car?.MPG
            vin.text = car?.VIN
            stock_num.text = car?.stockNum



            controller.launch(Dispatchers.Main){






                val bitmap = controller.fetchImage(car.imageUrl)
                carImage.setImageBitmap(bitmap)
                carImage.visibility = View.VISIBLE

            }


        }
    }





    companion object {
        val TAG = ViewCarFragment::class.java.simpleName

        val PERMISSION_REQUEST = 1
    }


}
