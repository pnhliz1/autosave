package edu.towson.cosc435.autosave.database

import androidx.room.*
import edu.towson.cosc435.autosave.MainActivity
import edu.towson.cosc435.autosave.models.Car
import java.util.*

@Dao
interface CarDao {
    @Insert
    suspend fun addCar(car: Car)

    @Update
    suspend fun updateCar(car: Car)

    @Query("select * from Car")
    suspend fun getCars(): List<Car>
}

class UUIDConverter {
    @TypeConverter
    fun fromString(uuid: String): UUID {
        return UUID.fromString(uuid)
    }

    @TypeConverter
    fun toString(uuid: UUID): String {
        return uuid.toString()
    }

    companion object {
        val TAG = UUIDConverter::class.java.simpleName


    }
}

@Database(entities = arrayOf(Car::class), version = 2, exportSchema = false)
@TypeConverters(UUIDConverter::class)
abstract class CarDatabase : RoomDatabase() {
    abstract fun carDao(): CarDao
}