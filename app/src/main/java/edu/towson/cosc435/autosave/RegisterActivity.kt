package edu.towson.cosc435.autosave

import android.content.Context
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_register.*
import android.text.method.TextKeyListener.clear
import android.R.id.edit
import android.content.Intent
import android.content.SharedPreferences.Editor
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T



class RegisterActivity  : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)




        var user_name = findViewById(R.id.username_register) as EditText
        var password = findViewById(R.id.password_register) as EditText

        var btn_submit = findViewById(R.id.submit_register) as Button

        // set on-click listener
        btn_submit.setOnClickListener {

            val sharePreferences = getSharedPreferences("LOGIN_INFO", Context.MODE_PRIVATE)
            val newUserName = user_name.editableText.toString();
            val newPassword = password.editableText.toString();
            //  Toast.makeText(this@RegisterActivity, user_name, Toast.LENGTH_LONG).show()
            val edit = sharePreferences.edit()
            // edit.clear()
            edit.putString("data",newUserName+newPassword)


            edit.apply()

            val loginScreen =  Intent(this@RegisterActivity, MainActivity::class.java )
            startActivity(loginScreen)

        }
    }

    //for unit testing
    companion object {
        val TAG = LoginActivity::class.java.simpleName


    }
}