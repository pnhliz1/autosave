package edu.towson.cosc435.autosave.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class Car (

    @PrimaryKey
    val id:UUID,
    @ColumnInfo(name = "make")
    val make: String,
    @ColumnInfo(name = "model")
    val model: String,
    @ColumnInfo(name = "bodyType")
    val bodyType:String,
    @ColumnInfo(name = "year")
    val year: String,
    @ColumnInfo(name = "imageUrl")
    val imageUrl:String,
    @ColumnInfo(name = "price")
    val price:String,
    @ColumnInfo(name = "mileage")
    val mileage:String,
    @ColumnInfo(name = "location")
    val location:String,
    @ColumnInfo(name = "color")
    val color:String,
    @ColumnInfo(name = "driveType")
    val driveType:String,
    @ColumnInfo(name = "transmission")
    val transmission:String,
    @ColumnInfo(name = "engine")
    val engine:String,
    @ColumnInfo(name="fuel")
    val fuel:String,
    @ColumnInfo(name = "MPG")
    val MPG:String,
    @ColumnInfo(name = "VIN")
    val VIN:String,
    @ColumnInfo(name = "stockNum")
    val stockNum:String,
    @ColumnInfo(name = "mechanicShop")
    val mechanic:String,
    @ColumnInfo(name = "carFax")
    val carFax:String,
    @ColumnInfo(name = "review")
    val review:String,
    @ColumnInfo(name="dealerPhone")
    val dealerPhone:String,
    @ColumnInfo(name="savedCars")
    val savedCars:Boolean



)
