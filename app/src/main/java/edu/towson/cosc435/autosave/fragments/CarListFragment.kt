package edu.towson.cosc435.autosave.fragments

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.navigation.NavigationView
import edu.towson.cosc435.autosave.R
import edu.towson.cosc435.autosave.adapters.CarAdapter
import edu.towson.cosc435.autosave.controllers.CarController
import kotlinx.android.synthetic.main.activity_car_list_fragment.*
import kotlinx.coroutines.launch
import java.lang.Exception

class CarListFragment : Fragment() {

    private lateinit var carController: CarController

    override fun onAttach(context: Context) {
        super.onAttach(context)

        when(context) {
            is CarController -> carController = context
            else -> throw Exception("CarController expected")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.activity_car_list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = CarAdapter(carController)

        recyclerView_fragment.adapter = adapter

        recyclerView_fragment.layoutManager = LinearLayoutManager(context)




    }

    override fun onResume() {
        super.onResume()

        carController.launch {
            carController.cars.GetCars()
            recyclerView_fragment?.adapter?.notifyDataSetChanged()
        }
    }
}

