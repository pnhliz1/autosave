package edu.towson.cosc435.autosave.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import edu.towson.cosc435.autosave.MainActivity
import edu.towson.cosc435.autosave.R
import edu.towson.cosc435.autosave.controllers.CarController
import edu.towson.cosc435.autosave.models.Car
import kotlinx.android.synthetic.main.listcar_layout.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception

class CarAdapter(val controller: CarController): RecyclerView.Adapter<CarViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.cardview_layout, parent, false)
        val vh = CarViewHolder(view)

        view.setOnClickListener {
            val position = vh.adapterPosition
            controller.viewCar(position)
        }



        return vh
    }

    override fun getItemCount(): Int {
        return controller.cars.GetCount()
    }

    override fun onBindViewHolder(holder: CarViewHolder, position: Int) {
        val car = controller.cars.GetCar(position)
        holder.bindCar(controller,car)

    }

    companion object {
        val TAG = CarAdapter::class.java.simpleName
    }
}

class CarViewHolder(view: View) : RecyclerView.ViewHolder(view){
    fun bindCar(controller:CarController,car:Car){

        itemView.car_make.text = car.make
        itemView.car_model.text = car.model
        itemView.car_year.text = car.year



        controller.launch(Dispatchers.Main){






            val bitmap = controller.fetchImage(car.imageUrl)
            itemView.carImage.setImageBitmap(bitmap)
            itemView.carImage.visibility = View.VISIBLE

        }
    }
}