package edu.towson.cosc435.autosave.repositories

import edu.towson.cosc435.autosave.models.Car
import java.util.*

interface CarRepository {
    suspend fun GetCars(): List<Car>
    fun GetCar(pos: Int): Car
    fun GetCount(): Int
    suspend fun addCar(car: Car)
    suspend fun replace(idx: Int, car: Car)

}

class CarMemRepository : CarRepository {


    override suspend fun addCar(song: Car) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }




    private val cars = mutableListOf<Car>()

    init {
        (0..5).forEach { i ->
            cars.add(Car(
                id = UUID.randomUUID(),
                make = "Make $i",
                model = "Model $i",
                bodyType = "bodyType $i",
                year = "Year $i",
                imageUrl = "imageUrl $i",
                price = "price $i",
                mileage = "mileage $i",
                location = "location $i",
                color = "gray $i",
                driveType = "driveType $i",
                transmission = "transmission $i",
                engine = "engine $i",
                fuel = "fuel $i",
                MPG = "MPG $i",
                VIN = "VIN $i",
                stockNum = "stockNum",
                mechanic = "mechanic $i",
                carFax = "carFax $i",
                review = "review $i",
                dealerPhone = "dealerPhone $i",
                savedCars = false



            ))
        }
    }


    suspend override fun GetCars(): List<Car> {
        return cars
    }

    override fun GetCar(pos: Int): Car {
       return cars.get(pos)
    }


    override fun GetCount(): Int {
        return cars.size
    }

    override suspend fun replace(idx: Int, car: Car) {
        if(idx >= cars.size) throw Exception("Outside of bounds")
        cars[idx] = car
    }
}