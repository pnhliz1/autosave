package edu.towson.cosc435.autosave.controllers

import android.graphics.Bitmap
import edu.towson.cosc435.autosave.models.Car
import edu.towson.cosc435.autosave.repositories.CarRepository
import kotlinx.coroutines.CoroutineScope

interface CarController: CoroutineScope {

    val cars: CarRepository
    suspend fun fetchImage(url:String): Bitmap
    suspend fun checkCache(image:String): Bitmap?
    suspend fun fetchCars():List<Car>
    fun getCarData(): Car?
    fun viewCar(idx: Int)
    suspend fun addNewCar(car:Car)
    fun launchMainScreen()



}