package edu.towson.cosc435.autosave

import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.core.app.NotificationManagerCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.navigation.NavigationView
import edu.towson.cosc435.autosave.adapters.CarAdapter
import edu.towson.cosc435.autosave.controllers.CarController
import edu.towson.cosc435.autosave.database.CarDatabaseRepository
import edu.towson.cosc435.autosave.models.Car
import edu.towson.cosc435.autosave.network.CarApi
import edu.towson.cosc435.autosave.repositories.CarMemRepository
import edu.towson.cosc435.autosave.repositories.CarRepository
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.coroutines.launch
import java.io.File
import kotlin.coroutines.CoroutineContext
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.observe
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_car_list_fragment.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.withContext

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener,CarController {

    override fun launchMainScreen() {
        if(resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            findNavController(R.id.nav_host_fragment)
                .navigate(R.id.action_carListFragment_to_viewCarFragment)
        }
    }

    override suspend fun addNewCar(car: Car) {
        try {
            withContext(Dispatchers.IO) {
                cars.addCar(car)
            }
            if(resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
                findNavController(R.id.nav_host_fragment)
                    .popBackStack()
            } else {
                // update the recyclerview
                recyclerView_fragment.adapter?.notifyDataSetChanged()
            }
        } catch (e: Exception) {
            Log.e(TAG, "Error: ${e}")
            Toast.makeText(this, "Failed to add new car", Toast.LENGTH_SHORT).show()
            throw e
        }
    }

    

    override fun getCarData(): Car? {
        return viewingCar
    }

    override fun viewCar(idx: Int) {
        val car = cars.GetCar(idx)
        viewingCar = car
        viewingCarIdx = idx

        if(resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            findNavController(R.id.nav_host_fragment)
                .navigate(R.id.action_carListFragment_to_viewCarFragment)
        }
    }





    override suspend fun fetchCars(): List<Car> {
        return carApi.fetchCars().await()
    }

    override suspend fun fetchImage(url: String): Bitmap {
        return carApi.fetchImage(url).await()
    }

    override suspend fun checkCache(image: String): Bitmap? {
        val file = File(cacheDir, image)
        if(file.exists()){
            val input = file.inputStream()
            return BitmapFactory.decodeStream(input)
        }else{
            return null
        }
    }



    override val coroutineContext: CoroutineContext
        get() = lifecycleScope.coroutineContext




    override lateinit var cars: CarRepository
    private var viewingCar: Car? = null
    private var viewingCarIdx: Int = -1
    private lateinit var carApi : CarApi
    private lateinit var rv:RecyclerView

    lateinit var toolbar: Toolbar
    lateinit var drawerLayout: DrawerLayout
    lateinit var navView: NavigationView

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_logout -> {
                val intent = Intent(this,LoginActivity::class.java)
                startActivity(intent)
                finish()
            }

        }
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        toolbar = findViewById(R.id.toolbar)


        drawerLayout = findViewById(R.id.drawer_layout)
        navView = findViewById(R.id.nav_view)

        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, R.string.filter_text, 0
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        navView.setNavigationItemSelectedListener(this)

        carApi = CarApi(this)

        cars = CarDatabaseRepository(this)


       launch {

            val carListAPI = fetchCars()
            val carListDB = cars.GetCars()
            carListAPI.forEach{carAPI ->
                if(carListDB.firstOrNull{carDB -> carDB.id == carAPI.id} == null){
                    cars.addCar(carAPI)
                }
            }


        }

        val scheduler = getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
        val jobInfo = JobInfo.Builder(JOB_ID, ComponentName(this, CarsService::class.java))
        jobInfo.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
        jobInfo.setMinimumLatency(10000)
        scheduler.schedule(jobInfo.build())

        MessageQueue.Channel.observe(this, { car ->
            Log.d(TAG, "Received new car from CarsService: $car")
            NotificationManagerCompat.from(this).cancel(CarsService.NOTIF_ID)
            launch(Dispatchers.IO) {
                (cars as CarDatabaseRepository).refreshCarList()
                withContext(Dispatchers.Main) {
                    recyclerView.adapter?.notifyDataSetChanged()
                }
            }
        })




    }


    override fun onStop() {
        super.onStop()
        this.cancel()
    }

    companion object {
        val TAG = MainActivity::class.java.simpleName
        val JOB_ID = 1

    }

}


