package edu.towson.cosc435.autosave.database

import android.content.Context
import androidx.room.Room
import edu.towson.cosc435.autosave.models.Car
import edu.towson.cosc435.autosave.repositories.CarRepository
import kotlinx.coroutines.delay
import java.lang.Exception

class CarDatabaseRepository(ctx: Context) : CarRepository {

    private val carList: MutableList<Car> = mutableListOf()
    private val db: CarDatabase

    init {
        db = Room.databaseBuilder(ctx,CarDatabase::class.java,"todos.db").allowMainThreadQueries().build()



    }

    override suspend fun replace(idx: Int, car: Car) {

        db.carDao().updateCar(car)
        refreshCarList()
    }

    override suspend fun GetCars(): List<Car> {
        if(carList.size == 0) {
            refreshCarList()
        }
        return carList
    }

    override fun GetCar(pos: Int): Car {
        return carList.get(pos)
    }



    override fun GetCount(): Int {
        return carList.size
    }



    override suspend fun addCar(car: Car) {

        db.carDao().addCar(car)
        refreshCarList()
    }


    suspend fun refreshCarList() {
        carList.clear()
        val cars = db.carDao().getCars()
        carList.addAll(cars)
    }
}