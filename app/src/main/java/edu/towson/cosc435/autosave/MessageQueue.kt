package edu.towson.cosc435.autosave

import androidx.lifecycle.MutableLiveData
import edu.towson.cosc435.autosave.models.Car

class MessageQueue {

    companion object {
        val Channel: MutableLiveData<Car> = MutableLiveData()
    }
}