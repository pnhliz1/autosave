package edu.towson.cosc435.autosave

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val et_username = findViewById(R.id.username_login) as EditText
        val et_password = findViewById(R.id.password_login) as EditText

        var btn_login = findViewById(R.id.login_btn) as Button
        var btn_register = findViewById(R.id.register_btn) as Button

        btn_login.setOnClickListener {


            val preferences = getSharedPreferences("LOGIN_INFO", Context.MODE_PRIVATE)
            val username = et_username.editableText.toString()
            val password = et_password.editableText.toString()

            val loginCombo = username+password

            val userDetails = preferences.getString("data",
                ""
            )
            if(loginCombo.equals(userDetails)){
                val login = Intent(this@LoginActivity, MainActivity::class.java)
                startActivity(login)
            }
            /*  val editor = preferences.edit()
              editor.putString("display", userDetails)
              editor.apply()*/


        }


        btn_register.setOnClickListener {
            val intent = Intent(this@LoginActivity,RegisterActivity::class.java)
            startActivity(intent)

        }

    }

    //for unit testing
    companion object {
        val TAG = LoginActivity::class.java.simpleName


    }
    }

