package edu.towson.cosc435.autosave

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.job.JobParameters
import android.app.job.JobService
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import edu.towson.cosc435.autosave.database.CarDatabaseRepository
import edu.towson.cosc435.autosave.models.Car
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*
import kotlin.coroutines.CoroutineContext

class CarsService:JobService(),CoroutineScope {

    override fun onStopJob(p0: JobParameters?): Boolean {
        return true
    }

    override fun onCreate() {
        super.onCreate()
        createNotificationChannel()
    }

    override fun onStartJob(p0: JobParameters?): Boolean {

        val carList: MutableList<Car> = mutableListOf()



        Log.d(TAG, "Cars Service job started")
        // fetch a new song from the web api...

        val car1 = Car(UUID.randomUUID(),"Hyundai","Elantra","sedan","2010","https://s.aolcdn.com/commerce/autodata/images/OOHYGEB1.jpg","$4500","0","Westminster,MD","blue","FWD","automatic","4 Cyl","gasoline","26/34","KMHDU4AD5AU178216","MU178216","","https://www.carfax.com/VehicleHistory/p/Report.cfx?partner=CUL_1&vin=KMHDU4AD5AU178216&compCode=VoWYTOapXJ9%2BjPyH24EUqd2yoNWDe6qc","https://www.edmunds.com/hyundai/elantra/2010/review/","(844) 909-1167",false)
        val car2 = Car(UUID.randomUUID(),"Mercury","Montego","sedan","2005","https://www.carspecs.us/photos/11981b808dacda5bc929d477cc8bfda593fba8c5-2000.jpg","$4290","97,200","Sykesville,MD","gray","AWD","automatic","6 Cyl","gasoline","17/23","1MEHM431X5G623416","180905","","https://www.carfax.com/VehicleHistory/p/Report.cfx?partner=CUL_1&vin=1MEHM431X5G623416&compCode=SYdvBSoMk%2BzAvUTZ6DIhjrkB%2FfhaAH%2Bg","https://www.edmunds.com/mercury/montego/2005/st-100456726/consumer-reviews/","(855) 955-0828",false)
        val car3 = Car(UUID.randomUUID(),"Ford","Taurus","sedan","2007","https://imagescdn.dealercarsearch.com/Media/12445/12640990/636911837134838238.jpg","$3995","73,608","Pasadena,MD","white","FWD","automatic","6 Cyl","gasoline","18/25","1FAFP56U97A153578","204009","","https://www.carfax.com/VehicleHistory/p/Report.cfx?partner=UCL_1&vin=1FAFP56U97A153578&compCode=Vml%2BM7wDZRThUjb5Q5dBAOpkqqAcoXyn","https://www.edmunds.com/ford/taurus/2007/consumer-reviews/","(410) 437-7300",false)
        val repo = CarDatabaseRepository(this)
        carList.add(car1)
        carList.add(car2)
        carList.add(car3)
        val rand_car  = carList.get((0..2).random())
        launch {

            repo.addCar(rand_car)
            val notif = createNotification()
            NotificationManagerCompat.from(this@CarsService).notify(NOTIF_ID, notif)
            MessageQueue.Channel.postValue(car1)


            sendBroadcast(Intent(MyReceiver.IMAGE_ACTION))
        }

        return true
    }

    private fun createNotificationChannel() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "AddedCarsChannel"
            val descriptionTxt = "Notification channel for updates on new cars being added."
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                description = descriptionTxt
            }

            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    private fun createNotification(): Notification {
        val intent = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        return NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentText("We got some sweet rides that will make your day! Why not check them out?")
            .setContentTitle("New Inventory!")
            .setSmallIcon(android.R.drawable.ic_dialog_info)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
            .build()
    }

    companion object {
        val TAG = CarsService::class.java.simpleName
        val CHANNEL_ID = "AddedCarsChannelId"
        val NOTIF_ID = 1
    }

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO


}