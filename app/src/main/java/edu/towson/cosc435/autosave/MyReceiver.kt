package edu.towson.cosc435.autosave

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import edu.towson.cosc435.autosave.controllers.CarController

class MyReceiver(val controller: CarController) : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        when(intent.action){
            IMAGE_ACTION -> {
                controller.launchMainScreen()
            }
        }
    }

    companion object{
        val IMAGE_ACTION = "CARS_AVAILABLE"
    }


}