package edu.towson.cosc435.autosave

import edu.towson.cosc435.autosave.adapters.CarAdapter
import edu.towson.cosc435.autosave.database.UUIDConverter
import org.junit.Assert.*
import org.junit.Test

class MainActivityVsUUIDTest{
    @Test
    fun testActivityNotNull() {

        assertTrue(MainActivity.equals(UUIDConverter))

    }
}