README

AutoSave

An app that allows user to purchase cars at affordable prices

User can login and view cars individually by checking their carfax, reviews, and can even call the dealer if they're interested in the vehicle.

The app also allows allows users to find mechanics near by opening an intent to Google Maps that puts the term "mechanics" in the search field so users can easily see available mechanics nearby.